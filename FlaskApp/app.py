from flask import Flask, render_template, flash, redirect,url_for,send_file, request, session, abort, json, jsonify

from Classes.LoginManager import LoginManager
from Classes.DealsManager import DealsManager
from Classes.PlaceManager import PlaceManager
from Classes.PasswordManager import PasswordManager
from Classes.DealFilters import DealFilters
from flask_mail import Mail, Message
from flask_pushjack import FlaskAPNS
import flask

from flask import Flask, render_template, flash, redirect, url_for, session, request, logging
from flask_mysqldb import MySQL

from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from functools import wraps

import json
import os
from Classes.DB import DB
from django.http import HttpResponse

app = Flask(__name__)
app.secret_key = os.urandom(12)

db = DB()
db.create_db()
# _dealsManager = DealsManager()

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'arcticfeb18@gmail.com'
app.config['MAIL_PASSWORD'] = 'chaklochaklo'
# app.config['MAIL_USERNAME'] = 'lunchbuxnyc1@gmail.com'
# app.config['MAIL_PASSWORD'] = 'lunchbux!123'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

app.config['APNS_CERTIFICATE'] = 'ssl/pushDist.pem' # production
#app.config['APNS_CERTIFICATE'] = 'ssl/apns.pem' # testing
apns = FlaskAPNS(app)

@app.route('/referral', methods = ['GET'])
def referral():
    return render_template('signup.html')

@app.route('/signup', methods = ['POST'])
def signup():
    return LoginManager().register(request = request)

@app.route('/signin', methods = ['POST'])
def signin():
    return LoginManager().signin(request = request)

@app.route('/location', methods = ['POST'])
def location():
    return LoginManager().add_device(request = request)

@app.route('/forgotpassword', methods = ['POST'])
def forgotpassword():
    return PasswordManager().forgatpasswordMes(request = request, mail = mail)

@app.route('/userProfileUpdate', methods = ['POST'])
def userProfileUpdate():
    return LoginManager().userProfileUpdate(request = request)

@app.route('/getProfile', methods = ['GET'])
def getProfile():
    return LoginManager().getProfile(request = request)

@app.route('/addToken', methods = ['POST'])
def addToken():
    return DealFilters().addToken(request = request)

@app.route('/addReview', methods = ['POST'])
def addReview():
    return DealFilters().addReview(request = request)

@app.route('/getReview', methods = ['GET'])
def getReview():
    return DealFilters().getReview(request = request)

@app.route('/getDeals', methods = ['GET'])
def getDeals():
    return DealFilters().getDeals(request = request)

@app.route('/getDealsByLatLng', methods = ['GET'])
def getDealsByLatLng():
    return DealFilters().getDealsByLatLng(request = request)

@app.route('/removeFavourites', methods = ['POST'])
def removeFavourites():
    return DealFilters().removeFavourites(request = request)

@app.route('/addFavourites', methods = ['POST'])
def addFavourites():
    return DealFilters().addFavourites(request = request)

@app.route('/getFavourites', methods = ['GET'])
def getFavourites():
    return DealFilters().getFavourites(request = request)

@app.route('/getTokens', methods = ['GET'])
def getTokens():
    return DealFilters().getTokens(request = request)

@app.route('/addClaims', methods = ['POST'])
def addToClaims():
    return DealFilters().addToClaims(request = request)

@app.route('/getClaims', methods = ['GET'])
def getClaims():
    return DealFilters().getClaims(request = request)
#"*******************************************"

@app.route("/dealimages/<imagename>", methods=['GET'])
def dealimages(imagename):
    print(imagename)
    return DealsManager().getDealImage(request = request, imageName = imagename)

@app.route('/login', methods=['POST'])
def do_admin_login():
    if request.form['password'] == '1unchB0x' and request.form['username'] == 'theDash':
        session['logged_in'] = True
    else:
        return render_template('login.html')
    return home()

@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return homepage()

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

@app.route('/home')
def homepage():
    if session.get('logged_in'):
        return render_template('homepage.html')
    else:
        return home()

@app.route('/addDeals', methods=['GET', 'POST'])
def addDeals():
    return DealsManager().addDeals(request = request)
    # if session.get('logged_in'):
    # else:
    #     return home()    

@app.route('/dealsinarea', methods=['POST'])
def dealsinarea():
    return DealsManager().dealsinarea(request = request)

@app.route('/getMyDeals')
def getMyDeals():
    response = DealsManager().getDeals(request = request)
    if response["status"] == "ok":
        return render_template('MyDeals.html', deals = response["deals"])
    else:
        #Show Error message
        return response
    
@app.route('/editDeal')
def editDeal():
    return DealsManager().editDeal(request = request)

@app.route('/updateDeal', methods=['POST'])
def updateDeal():
    return DealsManager().updateDeal(request = request)
    
@app.route('/deleteDeal')
def deleteDeal():
    response = DealsManager().deleteDeal(request = request)
    if response["status"] == "ok":
        return getMyDeals()
    else:
        #Show Error message
        return response["message"]

@app.route('/addcategories', methods=['POST','GET'])
def addcategories():
    if flask.request.method == "POST":
        return PlaceManager().addcategories(request = request)
    else:
        return getcategories()

@app.route('/getcategories')
def getcategories():
    if session.get('logged_in'):
        response = PlaceManager().getAllcategories(request = request)
        if response["status"] == "ok":
            return render_template("addcategories.html", categories = response["categories"])
        else:
            #Show Error message
            return response["message"]
    else:
        return home()


@app.route('/deletecategory')
def deletecategory():
    if session.get('logged_in'):
        response = PlaceManager().deletecategory(request = request)
        if response["status"] == "ok":
            return addcategories()
        else:
            #Show Error message
            return response["message"]
    else:
        return home() 
    
@app.route('/showAllUsers', methods=['POST','GET'])
def showAllUsers():
    if flask.request.method == "POST":
        LoginManager().deleteUser(request = request)
    if session.get('logged_in'):
        response = LoginManager().getAllUsers(request = request)
        if response["status"] == "ok":
            return render_template('Users.html', deals = response["data"])
        else:
            #Show Error message
            return response["message"]
    else:
        return home()

@app.route('/addfilters', methods=['POST','GET'])
def addfilters():
    if flask.request.method == "POST":
        return DealFilters().addfilters(request = request)
    else:
        return getfilters()


@app.route('/getfilters')
def getfilters():
    response = DealFilters().getAllfilters(request = request)
    if response["status"] == "ok":
        return render_template("addfilters.html", filters = response["filters"])
    else:
        #Show Error message
        return response["message"]

@app.route('/deletefilter')
def deletefilter():
    response = DealFilters().deletefilter(request = request)
    if response["status"] == "ok":
        return addfilters()
    else:
        #Show Error message
        return response["message"]

@app.route('/getClaimedDeals', methods = ['GET'])
def getClaimedDeals():
    return DealFilters().getClaimedDeals(request = request)

@app.route('/getClaimUsers', methods = ['GET'])
def getClaimUsers():
    return DealFilters().getClaimUsers(request = request)

@app.route('/redeemPoints', methods = ['POST'])
def redeemPoints():
    return DealsManager().redeemPoints(request = request, mail = mail)

@app.route('/addPayment', methods = ['POST'])
def addToPayments():
    return DealFilters().addToPayments(request = request)

@app.route('/getPaidUsers', methods = ['GET'])
def getPaidUsers():
    return DealFilters().getPaidUsers(request = request)

@app.route('/pay', methods = ['POST'])
def pay():
    return DealsManager().pay(request = request)

@app.route('/redeemCode', methods = ['POST'])
def redeemCode():
    return LoginManager().redeemCode(request = request)


if __name__ == "__main__":
#    app.secret_key = os.urandom(12)
    app.debug = True
    app.run(host='127.0.0.1',port=8000)

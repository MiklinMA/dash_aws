from flask import Flask, render_template, flash, redirect,url_for, request,send_file, session, abort, json, jsonify
import flask
from pushjack import APNSClient, GCMClient
from flask_mail import Mail, Message

import json
from datetime import datetime, timedelta
from Classes.PlaceManager import PlaceManager
from Classes.DealFilters import DealFilters
from django.http import HttpResponse
import mysql.connector
from mysql.connector import errorcode
import DB
import pdb
import os
import time
import math
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
import requests
#key = "AIzaSyBeq-alRFrMUeR0KYHmKiF1kB4NPtEIhH0"
key = "AIzaSyC_l1t_2nwwdmwK5VG5ys07bnfEwp231TQ"
search_url = "https://maps.googleapis.com/maps/api/place/textsearch/json"
details_url = "https://maps.googleapis.com/maps/api/place/details/json"
seconds = time.time()

admin_email = 'choonmatthew@gmail.com'
# admin_email = 'MiklinMA@gmail.com'

apns = APNSClient(certificate='ssl/apns.pem',
                    default_error_timeout=10,
                    default_expiration_offset=2592000,
                    default_batch_size=100,
                    default_retries=5)
gcm = GCMClient(api_key='ssl/apns.pem')

import stripe

class DealsManager:

    def notify(self, msg, lat=None, lng=None, rad=5):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        connection.database = DB.DB_NAME
        query = 'SELECT * FROM devices WHERE 1 = 1'

        if lat is not None:
            ln = int(float(lat))
            lnmin = ln-rad
            lnmax = ln+rad
            query += " AND latitude BETWEEN '%d' AND '%d'" % (lnmin, lnmax)
        if lng is not None:
            lg = int(float(lng))
            lgmin = lg-rad
            lgmax = lg+rad
            query += " AND latitude BETWEEN '%d' AND '%d'" % (lnmin, lnmax)

        cursor = connection.cursor(dictionary=True)
        cursor.execute(query)
        rows = cursor.fetchall()
        for row in rows:
            token = row.get('token')
            if not token:
                continue

            print 'PUSH', row.get('os'), token, msg
            try:
                if row.get('os') == 'ios':
                    apns.send(token, msg)
                else:
                    gcm.send(token, msg)
            except Exception as e:
                print "PUSH ERROR", e

        cursor.close()

    def renderAddDealPage(self, request):
        filterResponse = DealFilters().getAllfilters(request = request)
        filters = filterResponse["filters"]
        return render_template('addDeals.html', filters = filters, deal = "")
     
    def addDeals(self, request):
        if flask.request.method == 'POST':
            return self.addDealsDB(request = request)
        else:
            return self.renderAddDealPage(request = request)
       
    def dealsinarea (self, request):
        zip = request.form['zip']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        # connection.database = "lunchbox"
        connection.database = DB.DB_NAME

        try:
            cursor.execute("SELECT * FROM deals WHERE zip=%s",(zip,))
            items = cursor.fetchall()
            cursor.close()
            connection.close()
        
            itemsDict = {"status": "ok",
                        "message": "Deals",
                        'deals': items
            }
            return jsonify(itemsDict)
        
        except Exception as e:
            cursor.close()
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def redeemPoints(self, request, mail):
        print("REDEEM", request.form)
        email = request.form.get('email')
        points = request.form.get('points', None)
        rtype = request.form.get('type', "default")
        err = {
            'status':'fail', 
            'message':'Request Error'
        }
        if not email:
            return jsonify(err)

        print(type(points), points)
        if isinstance(points, (unicode, str)):
            if points.isdigit():
                points = int(points)
            else:
                return jsonify(err)

        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = DB.DB_NAME

        try:
            cursor.execute("SELECT points FROM users WHERE email=%s",(email,))
            res = cursor.fetchone()
            if (points is None and int(res['points']) <= 1500) or int(res['points']) < points:
                err['message'] = "It will work only after 1500 points"
                err['points'] = res['points']
                return jsonify(err)

            cursor.execute("INSERT INTO redeems (user_email, points,rtype) VALUES (%s, %s, %s)" % (
                email, res['points'], rtype
                )
            )
            connection.commit()

            if points is None:
                points = 0
            else:
                points = int(res['points']) - points

            cursor.execute("UPDATE users SET points=%d WHERE email = '%s'" % (points, email))
            connection.commit()
            cursor.close()
            connection.close()

            msg = Message('Redeem Points', 
                        sender = 'arcticfeb18@gmail.com',
                        recipients = [admin_email])
            msg.body = "Hello, Here is new redeem request from %s (%d points)" % (email, res['points'])
            mail.send(msg)
        
            itemsDict = {"status": "ok",
                        # "message": "Your points are redeemed admin will contact you ASAP",
                        # "message": "We will need to show thanks for redeem your point. We will get back to you asap.",
                        "message": "We will get back to you asap.",
                        'points': points
            }
            return jsonify(itemsDict)
        
        except Exception as e:
            print(e)
            cursor.close()
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })
    
    def addDealsDB(self, request):
        
        print ("addDealsDB called")

        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"        
        imageurl=""
        title = request.form['title']
        latitude = request.form.get('latitude')
        longitude = request.form.get('longitude')
        location = request.form.get('location')

        path = APP_ROOT
        target = path.replace('Classes','static/dealimages')
        imageName = None
        if not os.path.isdir(target):
            os.mkdir(target)
        for file in request.files.getlist("file"):
            if file.filename :
                imageName = title + str(seconds) + file.filename
                imageName = imageName.replace(' ', '')
                destination = "/".join([target,imageName])
                print("Destination ==", destination)
                file.save(destination)
                imageurl = imageName

        try:  
            description= request.form['description']
            price = request.form['price']
            buy_price = request.form['buy_price']
            image = imageurl
            if imageurl == "":
                image = request.form['image']
            timeleft = request.form['timeleft']
            print("############",timeleft)
            hours = int(float(timeleft))
            hoursinsec = hours * 3600
            minutes = (float(timeleft) - int(hours)) * 100
            mininsec = minutes * 60
            # timestamp = seconds+hoursinsec+mininsec
            timestamp = datetime.now() + timedelta(days=int(float(timeleft)))
            timestamp = time.mktime(timestamp.timetuple())
            print("***********", seconds,timestamp)
            offer = request.form['offer']
            type = request.form['type']
            filter_id = request.form['filter_id']
            
            cursor.execute("INSERT INTO deals (title,description,buy_price,price,offer,type,filter_id,location,latitude,longitude,image,timeleft) VALUES (%s, %s ,%s ,%s ,%s ,%s ,%s ,%s,%s,%s,%s,%s)",(title,description,buy_price,price,offer,type,filter_id,location,latitude,longitude,image,timestamp))
            connection.commit()
            cursor.close()
            connection.close()

            self.notify('add deal', latitude, longitude)

            flash('Deal added successfully', 'success')
            return self.renderAddDealPage(request = request)
    
        except Exception as e:
            print(e)
            cursor.close()
            connection.close()
            flash(str(e), 'error')
            if imageName is not None:
                os.remove(os.path.join(target,imageName))
            return self.renderAddDealPage(request = request)

    def getDealImage(self, request, imageName):
        print("Image Name = " + imageName)
        path = APP_ROOT
        target = path.replace('Classes','static/dealimages/')
        filename = target + imageName
        print ("filename = " + filename)
        return send_file(filename, mimetype='image/*')
 
    def editDeal(self, request):
        
        id = request.args['edit']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        try:
            cursor.execute("SELECT * FROM deals WHERE id=%s",(id,))
            deal = cursor.fetchone()
            cursor.close()
            connection.close()
            filterResponse = DealFilters().getAllfilters(request = request)
            filters = filterResponse["filters"]
            return render_template('addDeals.html', filters = filters, deal = deal)
        
        except Exception as e:
            print(e)
            return jsonify({
                    "status": "fail",
                    "message": "No records found."
                })
        
        return self.addDeals(request = request)

    def getDeals(self, request):
    
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        try:
            cursor.execute("SELECT * FROM deals ORDER BY date DESC")
            items = cursor.fetchall()            
            cursor.close()
            connection.close()
            return {"status": "ok",
                    "message": "deals",
                    "deals": items
                    }
        except Exception as e:
            cursor.close()
            return {"status": "fail",
                    "message": str(e)
                }         

    def deleteDeal(self, request):
        
        id = request.args['delete']
        print(id)
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        print(" Delete this Deal: " + id )
        try:
            cursor.execute("SELECT image FROM deals WHERE id=%s ",(id,))
            image = cursor.fetchone()
            print(image["image"])
            path = APP_ROOT
            target = path.replace('Classes','static/dealimages')
            os.remove(os.path.join(target,image["image"]))
        except Exception as e:
            print(e)
        try:
            cursor.execute("DELETE FROM deals WHERE id=%s",(id,))
            connection.commit()
            cursor.close()
            connection.close()
            flash('Deal Deleted successfully', 'success')   
            return {'status':'ok', 'message':'Your Deal is deleted successfully'}
            
        except Exception as e:
            print(e)
            return {
                    "status": "fail",
                    "message": "No records found with ID"
            }    

    def updateDeal(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"        
        imageurl=""
        id = request.form.get('id')       
        title = request.form.get('title')
        latitude = request.form.get('latitude')
        longitude = request.form.get('longitude')
        location = request.form.get('location')
        path = APP_ROOT
        target = path.replace('Classes','static/dealimages')
        imageName = None
        if not os.path.isdir(target):
            os.mkdir(target)
        for file in request.files.getlist("file"):
            if file.filename :
                imageName = title + str(seconds) + file.filename
                imageName = imageName.replace(' ', '')
                destination = "/".join([target,imageName])
                print("Destination ==")
                print(destination)
                file.save(destination)
                imageurl = imageName

        try:  
            description= request.form.get('description')
            price = request.form.get('price')
            buy_price = request.form['buy_price']
            print("IMAGE:", request.form.get('image'), imageurl)
            image = imageurl
            if imageurl == "":
                image = request.form.get('image')
            timeleft = request.form.get('timeleft')
            hours = int(float(timeleft))
            hoursinsec = hours * 3600
            minutes = (float(timeleft) - int(hours)) * 100
            mininsec = minutes * 60
            # timestamp = seconds+hoursinsec+mininsec
            timestamp = datetime.now() + timedelta(days=int(float(timeleft)))
            timestamp = time.mktime(timestamp.timetuple())
            print seconds,timestamp
            offer = request.form.get('offer')
            type = request.form.get('type')
            filter_id = request.form.get('filter_id')
            print image
            cursor.execute("UPDATE deals SET title=%s,description=%s,buy_price=%s,price=%s,offer=%s,type=%s,filter_id=%s,location=%s,latitude=%s,longitude=%s,image=%s,timeleft=%s WHERE id=%s",(title,description,buy_price,price,offer,type,filter_id,location,latitude,longitude,image,timestamp,id))
            connection.commit()
            cursor.close()
            connection.close()
            flash('Deal Updated successfully', 'success')
            return self.renderAddDealPage(request = request)
    
        except Exception as e:
            print(e)
            cursor.close()
            connection.close()
            flash(str(e), 'error')
            if imageName is not None:
                os.remove(os.path.join(target,imageName))
            return self.renderAddDealPage(request = request)

    def pay(self, request):
        try:
            # stripe.api_key = "pk_test_m14VfXicQgtQ0bRsY5BuOab9"
            stripe.api_key = "sk_test_MruoScOUXgNxSYnhzqIr7UqH"
        except Exception as e:
            return jsonify({
                "status": "fail",
                "message": "Wrong argument: %s" % e
            })

        try:
            data = request.get_json(force=True)

            print data

            token = data['stripeToken']
            amount = int(float(data['amount']) * 100)
            description = data['description']
        except Exception as e:
            return jsonify({
                "status": "fail",
                "message": "Wrong argument: %s" % e
            })    

        try:
            charge = stripe.Charge.create(
              amount=amount,
              currency="usd",
              card=token,
              description=description
            )
        except Exception as e:
            return jsonify({
                "status": "fail",
                "message": "Charge error: %s" % e
            })

        return jsonify({
            "status": "ok",
            "message": "Payment successful",
            "tid": charge.get('balance_transaction')
        })    


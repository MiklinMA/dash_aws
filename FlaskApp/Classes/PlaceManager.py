from flask import Flask, render_template, flash, redirect,url_for, request, session, abort, json, jsonify,send_from_directory, send_file

import json
from django.http import HttpResponse
import mysql.connector
from mysql.connector import errorcode
import DB
import os
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
class PlaceManager:
 
    
    def addcategories(self, request):
    
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        try:
            name = request.form['name']
            
            cursor.execute("INSERT INTO categories(name) VALUES (%s)",(name,))
            connection.commit()
            cursor.close()
            connection.close()
            flash('Category added', 'success')
            return redirect(url_for('getcategories'))
        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })
    def getAllcategories(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("SELECT * FROM categories ORDER BY date DESC")
            data = cursor.fetchall()
            cursor.close()
            connection.close()
            print(data)
            return {"status": "ok",
                    "message": "categories",
                    "categories": data
                    }
        except:
            cursor.close()
            return {"status": "fail",
                    "message": "Could'nt Load the categoris."} 

    def deletecategory(self, request):
        
        name = request.args['delete']
        print(name)
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        print(" Delete this Deal: " + name )
        try:
            cursor.execute("DELETE FROM categories WHERE name=%s",(name,))
            connection.commit()
            cursor.close()
            connection.close()
            flash('Category Deleted successfully', 'success')            
            return {'status':'ok', 'message':'Your category is deleted successfully'}
            
        except Exception as e:
            print(e)
            return {
                    "status": "fail",
                    "message": "No records found with ID"
            }
           
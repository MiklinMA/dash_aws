import mysql.connector
from mysql.connector import errorcode

DB_USER = "root"
DB_NAME = "lunchbox"
DB_HOST = "localhost"
DB_PASSWORD = "admin"


TABLES = []

#------------------* Users Table *-------------------
userTable = {}
userTable['name'] = 'users'
userTable['query'] = ('''
CREATE TABLE users (
	email VARCHAR(100) primary key,
	password VARCHAR(100),
	link VARCHAR(10),
	refer VARCHAR(10),
	name VARCHAR(100),
	image VARCHAR(500),
	phone VARCHAR(45),
	about VARCHAR(500),
	location VARCHAR(250),
	first_name VARCHAR(45),
	last_name VARCHAR(45),
	gender VARCHAR(45),
	points int NOT NULL DEFAULT '0',
	date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
''')
TABLES.append(userTable)
#-----------------------------------------------

filtersTable = {}
filtersTable['name'] = 'filters'
filtersTable['query'] = ("CREATE TABLE filters (filter_id int AUTO_INCREMENT PRIMARY KEY,filter_name VARCHAR(100))")
TABLES.append(filtersTable)
#-----------------------------------------------

dealsTable = {}
dealsTable['name'] = 'deals'
dealsTable['query'] = ("CREATE TABLE deals (title VARCHAR(100), description VARCHAR(255), buy_price VARCHAR(100), price VARCHAR(100), offer VARCHAR(100), type VARCHAR(100), filter_id int , zip VARCHAR(100), location VARCHAR(100), latitude double, longitude double, image VARCHAR(255), timeleft VARCHAR(100), ratings int, stars int, id INT AUTO_INCREMENT primary key NOT NULL, date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,CONSTRAINT fk_dealfilter FOREIGN KEY(filter_id) REFERENCES filters(filter_id) ON DELETE CASCADE )")
TABLES.append(dealsTable)

#-----------------------------------------------
categoriesTable = {}
categoriesTable['name'] = 'categories'
categoriesTable['query'] = ("CREATE TABLE categories (categories_id int AUTO_INCREMENT PRIMARY KEY, name VARCHAR(100), date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)")
TABLES.append(categoriesTable)

#-----------------------------------------------
favouritesTable = {}
favouritesTable['name'] = 'favourites'
favouritesTable['query'] = ("CREATE TABLE favourites (favourites_id int AUTO_INCREMENT primary key, user_email varchar(100), deal_id int, CONSTRAINT fk_favouriteuser FOREIGN KEY(user_email) REFERENCES users(email) ON DELETE CASCADE, CONSTRAINT fk_favouritedeal FOREIGN KEY(deal_id) REFERENCES deals(id) ON DELETE CASCADE )")
TABLES.append(favouritesTable)

#-----------------------------------------------
claimsTable = {}
claimsTable['name'] = 'claims'
claimsTable['query'] = ("CREATE TABLE claims (claim_id int AUTO_INCREMENT PRIMARY KEY, user_email VARCHAR(100), deal_id int, date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,CONSTRAINT fk_claimuser FOREIGN KEY(user_email) REFERENCES users(email) ON DELETE CASCADE,CONSTRAINT fk_claimdeal FOREIGN KEY(deal_id) REFERENCES deals(id) ON DELETE CASCADE)")
TABLES.append(claimsTable)

#-----------------------------------------------
reviewsTable = {}
reviewsTable['name'] = 'reviews'
reviewsTable['query'] = ("CREATE TABLE reviews (review_id int AUTO_INCREMENT PRIMARY KEY, user_email VARCHAR(100), deal_id int, stars int, title VARCHAR(100), description VARCHAR(100), date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,CONSTRAINT fk_reviewuser FOREIGN KEY(user_email) REFERENCES users(email) ON DELETE CASCADE,CONSTRAINT fk_reviewdeal FOREIGN KEY(deal_id) REFERENCES deals(id) ON DELETE CASCADE)")
TABLES.append(reviewsTable)

#-----------------------------------------------
tokensTable = {}
tokensTable['name'] = 'tokens'
tokensTable['query'] = ("CREATE TABLE tokens (token_id int AUTO_INCREMENT PRIMARY KEY, user_email VARCHAR(100), deal_id int, points int, shared boolean,CONSTRAINT fk_tokenuser FOREIGN KEY(user_email) REFERENCES users(email) ON DELETE CASCADE,CONSTRAINT fk_tokendeal FOREIGN KEY(deal_id) REFERENCES deals(id) ON DELETE CASCADE)")
TABLES.append(tokensTable)

#-----------------------------------------------
deviceTable = {}
deviceTable['name'] = 'devices'
deviceTable['query'] = ('''CREATE TABLE devices (
        device_id int AUTO_INCREMENT PRIMARY KEY,
        user_email VARCHAR(100),
        token VARCHAR(100) UNIQUE,
        os VARCHAR(10),
        latitude double,
        longitude double, 
        CONSTRAINT fk_deviceuser FOREIGN KEY(user_email) REFERENCES users(email) ON DELETE CASCADE
    )
''')
TABLES.append(deviceTable)

#-----------------------------------------------
paidTable = {}
paidTable['name'] = 'payments'
paidTable['query'] = ('''
CREATE TABLE payments (
	id int AUTO_INCREMENT PRIMARY KEY,
	user_email VARCHAR(100),
	deal_id int,
	price float,
	date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT fk_paiduser FOREIGN KEY(user_email) REFERENCES users(email) ON DELETE CASCADE,
CONSTRAINT fk_paiddeal FOREIGN KEY(deal_id) REFERENCES deals(id) ON DELETE CASCADE
)''')
TABLES.append(paidTable)

#-----------------------------------------------
redeemTable = {}
redeemTable['name'] = 'redeems'
redeemTable['query'] = ('''CREATE TABLE redeems (
	id int AUTO_INCREMENT PRIMARY KEY,
	user_email VARCHAR(100),
	points int,
	rtype VARCHAR(100),
CONSTRAINT fk_redeemuser FOREIGN KEY(user_email) REFERENCES users(email) ON DELETE CASCADE
)''')
TABLES.append(tokensTable)



connection = mysql.connector.connect(
    user=DB_USER,
    password=DB_PASSWORD,
    database=DB_NAME,
    host = DB_HOST
)

connection.close()

class DB:

    connection = mysql.connector.connect(user=DB_USER, password=DB_PASSWORD)
    cursor = connection.cursor(dictionary=True)
    NEED_TO_DROP_TABLES = False

    def create_db(self):
        self.connection.database = DB_NAME

        # Droping the table if nessessary
        #-----------------------------------------------
        if self.NEED_TO_DROP_TABLES == True:
            
            self.cursor.execute(" ALTER TABLE users ADD COLUMN points int NOT NULL DEFAULT '0' ")

            
        self.NEED_TO_DROP_TABLES = False
        #-----------------------------------------------

        # Creating new tables if not exist
        #-----------------------------------------------
        for tableDict in TABLES:
            try:
                name = tableDict['name']
                query = tableDict['query']
                print("Creating table {}: ".format(name))
                self.cursor.execute(query)
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                    print("already exists.")
                else:
                    print(err.msg)
        #-----------------------------------------------   

        self.cursor.close()
        connection.close()
        print ("exeucted this line")


from flask import Flask, render_template, flash, redirect, request, session, abort, json, jsonify

import json
import random
import string

from flask import Flask, render_template, flash, redirect, url_for, session, request, logging
from flask_mysqldb import MySQL
from passlib.hash import sha256_crypt
import getpass
from functools import wraps
import mysql.connector
import DB

from DealsManager import apns, gcm

class LoginManager:

    def add_device(self, request, _connection=None, email=None):
        token = request.form.get('token')
        dev_os = request.form.get('os')
        latitude = request.form.get('latitude')
        longitude = request.form.get('longitude')
        email = email or request.form.get('email')
        email = email and "'%s'" % email or 'NULL'

        print 'ARGS', request.args
        print 'FORM', request.form

        if _connection is None:
            connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
            connection.database = DB.DB_NAME
        else:
            connection = _connection

	err = jsonify({
            'message': 'Wrong data',
            'status': 'Error',
	})

	if token is None: return err
	if dev_os is None: return err
	if latitude is None: return err
	if longitude is None: return err

        cursor = connection.cursor(dictionary=True)
        query = '''INSERT INTO devices
                (user_email, token, os, latitude, longitude)
                VALUES (%s, '%s', '%s', '%s', '%s')
                ON DUPLICATE KEY UPDATE
                user_email=%s, token='%s', os='%s', latitude='%s', longitude='%s'
        ''' % (
                email, token, dev_os, latitude, longitude,
                email, token, dev_os, latitude, longitude
                )
        # cursor.execute('''INSERT INTO devices 
        #                 (user_email, token, os, latitude, longitude) 
        #                 VALUES (%s, %s, %s, %s)
        #                 ON DUPLICATE KEY UPDATE
        #                 token=%s''',(email, token, dev_os, latitude, longitude, token))
        print(query)                                                                                                                     
        cursor.execute(query)
        connection.commit()
        cursor.close()
        if _connection is None:
            connection.close()
        return jsonify({
            'message': 'Location changed',
            'status': 'OK',
        })
    
    def signin(self, request):
        email = request.form['email']        
        password_candidate = request.form['password']
 
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        print 'signin', request.form, request.args

        try:
            cursor.execute("SELECT * FROM users WHERE email=%s",(email,))
            data = cursor.fetchone()
            password = data["password"]
            useremail = data["email"]
            cursor.close()
            # hash = sha256_crypt.hash(password_candidate, password)
            if sha256_crypt.verify(password_candidate, password):
                self.add_device(request, connection, useremail)
                connection.close()
                return jsonify({                   
                            "status": "OK",                    
                            "message": "Signin Sucessfull",
                            "data": useremail                            
                        })                        
            else:               
                connection.close()
                return jsonify({                   
                            "status": "fail",                    
                            "message": "Password is not maching."               
                        })        
        except Exception as e:
            print(e)
            return jsonify({                    
                        "status": "fail",
                        "message": "No records found with email. Please Sign Up"
                        }) 

    def register(self, request):
        
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        print 'signup', request.form, request.args

        try:

            email = request.form['email']
            password = sha256_crypt.encrypt(str(request.form['password']))
            link = request.form.get('link')

            cursor.execute("SELECT * FROM users WHERE email = %s", (email,))
            result = cursor.fetchone()
            if result > 0:
                return jsonify({
                        "status":"fail",
                        "message": "This email already registered , please choose another "
                    })
            else:           
		new_link = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))

                cursor.execute("INSERT INTO users(email,password,link) VALUES (%s ,%s, %s)",(email,password, new_link))
		connection.commit()

                if link:
			query = "UPDATE users SET points = points + 300 WHERE link = '%s'" % link
			cursor.execute(query)
			connection.commit()

			query = "SELECT devices.* FROM users "
			query += "INNER JOIN devices ON devices.user_email = users.email "
			query += "WHERE users.link = '%s'" % link
			cursor.execute(query)
			rows = cursor.fetchall()
			for row in rows:
			    token = row.get('token')
			    if not token:
				continue

			    msg = 'Congratulations on receiving a referral bonus of 300 bolt points.'
			    print 'PUSH', row.get('os'), token, msg
			    try:
				    if row.get('os') == 'ios':
					apns.send(token, msg)
				    else:
					gcm.send(token, msg)
			    except:
				    pass

			print("Add points by referral link", query)


                cursor.close()
                self.add_device(request, connection, email)
                connection.close()

		if link:
			session['registered'] = True
			return redirect("http://admin.boltapp.co/referral")
			# return redirect("https://itunes.apple.com/us/app/bolt-claim-deals-get-reward/id1308341974")

                return jsonify({
                            "status": "ok",
                            "message": "SignUp Successful"
                        })

        except Exception as e:
            cursor.close()
            connection.close()
            print(e)
            return jsonify({
                        "status": "fail",
                        "error": "DB Error",
                        "message": "Could'nt SignUp now, Please try again later."
                    })                

    def getAllUsers(self, request):

        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        try:
            cursor.execute("SELECT * FROM users ORDER BY points DESC")
            items = cursor.fetchall()  
            usersDict = {"status": "ok",
                        "message": "Users",
                        'data': items
                        }
            cursor.close()
            connection.close()
            return usersDict

        except Exception as e:
            print(e)
            return e
            connection.close()
            return {"status": "fail",
                    "message": "Could'nt Load the users."}           

    def deleteUser(self, request):

        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        try:

            email = request.form['delete']   
            query = "DELETE FROM users WHERE email = %s"
            cursor.execute(query, (email,))
            connection.commit()
            cursor.close()
            connection.close()
            flash('User Deleted successfully', 'success')              
            usersDict = {"status": "ok",
                        "message": "User deleted",
                        }
            return usersDict
        
        except:
            cursor.close()
            connection.close()
            return {"status": "fail",
                    "message": "Could'nt delete the user."}           

    def userProfileUpdate(self, request): 
        email = request.form['email']       
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"      
        cursor.execute("SELECT * FROM users WHERE email=%s",(email,))
        data = cursor.fetchone()
        try: 
            first_name = request.form.get("first_name")
            last_name = request.form.get("last_name")
            gender = request.form.get("gender")          
            image = request.form.get("image")
            phone = request.form.get("phone")
            location = request.form.get("location")
            about = request.form.get("about")
            cursor.execute("UPDATE users SET first_name=%s, last_name=%s, gender=%s,image=%s,phone=%s,location=%s,about=%s WHERE email = %s",(first_name,last_name,gender,image, phone,location,about,email))
            connection.commit()
            cursor.execute("SELECT email,first_name,last_name,image,location,phone,about,date,gender FROM users WHERE email = %s",(email,))
            data = cursor.fetchone()
            cursor.close()
            connection.close()
            return jsonify({
                'status':'ok', 
                'message':'profile uploaded',
                'data':data
            })
        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getProfile(self, request):
        email = request.args['email']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("SELECT email,link,first_name,last_name,image,location,phone,about,date,gender,points FROM users WHERE email = %s",(email,))
            user = cursor.fetchone()
            cursor.execute("SELECT * FROM tokens WHERE user_email = %s",(email,))
            tokenData = cursor.fetchall()
            cursor.execute("SELECT * FROM favourites WHERE user_email = %s",(email,))
            favouriteData = cursor.fetchall()
            cursor.execute("SELECT * FROM claims WHERE user_email = %s", (email,))
            claimData = cursor.fetchall()
            cursor.execute("SELECT * FROM payments WHERE user_email = %s", (email,))
            paidData = cursor.fetchall()
            # cursor.execute(" SELECT * FROM deals where id IN (SELECT deal_id FROM favourites WHERE user_email = %s )",(email,))
            # favouriteDeals = cursor.fetchall()
            user["tokens"] = tokenData
            user["favourites"] = favouriteData
            user["claims"] = claimData
            user["payments"] = paidData
            # user["favouriteDeals"] = favouriteDeals

            usersDict = {
                "status": "ok",
                "message": "Users",
                'data': user,
                }
            cursor.close()
            connection.close()
            return jsonify(usersDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })
                  
    def redeemCode(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        print 'redeem code', request.form, request.args

        try:

            email = request.form['email']
            link = request.form['code']

            cursor.execute("SELECT * FROM users WHERE email = %s AND refer IS NULL", (email,))
            result = cursor.fetchone()
            if result is None:
                return jsonify({
                        "status":"fail",
                        "message": "Wrong email, please choose another."
                    })
            else:           
		query = "UPDATE users SET points = points + 300 WHERE link = '%s'" % link
		cursor.execute(query)
		connection.commit()

		query = "UPDATE users SET refer = '%s' WHERE email = '%s'" % (link, email)
		cursor.execute(query)
		connection.commit()

		query = "SELECT devices.* FROM users "
		query += "INNER JOIN devices ON devices.user_email = users.email "
		query += "WHERE users.link = '%s'" % link
		cursor.execute(query)
		rows = cursor.fetchall()
		for row in rows:
		    token = row.get('token')
		    if not token:
			continue

		    msg = 'Congratulations on receiving a referral bonus of 300 bolt points.'
		    print 'PUSH', row.get('os'), token, msg
		    try:
			    if row.get('os') == 'ios':
				apns.send(token, msg)
			    else:
				gcm.send(token, msg)
		    except:
			    pass

		print("Add points by referral link", query)


                cursor.close()
                self.add_device(request, connection, email)
                connection.close()

                return jsonify({
                            "status": "ok",
                            "message": "Redeem Successful"
                        })

        except Exception as e:
            cursor.close()
            connection.close()
            print(e)
            return jsonify({
                        "status": "fail",
                        "error": "DB Error",
                        "message": "Could'nt SignUp now, Please try again later."
                    })                



from flask import Flask, render_template, flash, redirect,url_for, request,send_file, session, abort, json, jsonify
import flask

import json
import datetime
from Classes.PlaceManager import PlaceManager
from django.http import HttpResponse
import mysql.connector
from mysql.connector import errorcode
import DB
import os

class DealFilters:
    
    def addfilters(self, request):
        
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        try:
            filter_name = request.form['filter_name']
            
            cursor.execute("INSERT INTO filters(filter_name) VALUES (%s)",(filter_name,))
            connection.commit()
            cursor.close()
            connection.close()
            flash('Filter added', 'success')
            return redirect(url_for('getfilters'))

        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getAllfilters(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("SELECT * FROM filters")
            data = cursor.fetchall()
            cursor.close()
            connection.close()
            return {"status": "ok",
                    "message": "filters",
                    "filters": data
                    }
        except:
            cursor.close()
            return {"status": "fail",
                    "message": "Could'nt Load the filters."
                    }

    def deletefilter(self, request):
        
        filter_id = request.args['delete']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("DELETE FROM filters WHERE filter_id = %s",(filter_id,))
            connection.commit()
            cursor.close()
            connection.close()
            flash('Filter Deleted successfully', 'success')            
            return {
                    'status':'ok', 
                    'message':'Your category is deleted successfully'
                    }
            
        except Exception as e:
            print(e)
            return {
                    "status": "fail",
                    "message": "No records found with ID"
            }

    def addReview(self, request):       
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            user_email = request.form['user_email']
            deal_id = request.form['deal_id']
            title = request.form['title']
            description = request.form['description']
            stars = request.form['stars']           
            cursor.execute("INSERT INTO reviews(user_email,deal_id,title,description,stars) VALUES (%s,%s,%s,%s,%s)",(user_email,deal_id,title,description,stars))
            
            cursor.execute("SELECT count(deal_id) from reviews WHERE deal_id = %s",(deal_id,))
            ratings = cursor.fetchone()
            ratingsCount = ratings["count(deal_id)"]

            cursor.execute("SELECT avg(stars) from reviews WHERE deal_id = %s",(deal_id,))
            stars = cursor.fetchone()
            avgStars = (stars["avg(stars)"])

            cursor.execute("UPDATE deals SET ratings=%s, stars=%s WHERE id=%s",(ratingsCount,avgStars,deal_id))
            connection.commit()
            cursor.close()
            connection.close()
            return jsonify({
                'status':'ok', 
                'message':'your review added'
            })
        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getReview(self, request):
        deal_id = request.args['deal_id']
        user_email = request.args['user_email']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("SELECT * FROM reviews WHERE deal_id = %s AND user_email = %s",(deal_id,user_email))
            dealReview = cursor.fetchall()
            reviewDict = {
                "status": "ok",
                "message": "dealReview",
                'data': dealReview,
                }
            cursor.close()
            connection.close()
            return jsonify(reviewDict)
        except Exception as e:
            print(e)
            return e
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def addToken(self, request):       
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            user_email = request.form['user_email']
            deal_id = request.form['deal_id']
            points = request.form['points']
            shared = request.form['shared']
            # cursor.execute("SELECT * FROM tokens WHERE deal_id = %s AND user_email = %s", (deal_id, user_email))
            # result = cursor.fetchone()
            result = 0
            if result > 0:
                return jsonify({
                        "status":"fail",
                        "message": "token already added to this deal "
                    })
            else:
                cursor.execute("INSERT INTO tokens(user_email,deal_id,points,shared) VALUES (%s,%s,%s,%s)",(user_email,deal_id,points,shared))
                cursor.execute("SELECT sum(points) from tokens WHERE user_email = %s",(user_email,))
                points = cursor.fetchone()
                totalPoints = (points["sum(points)"])
                cursor.execute("UPDATE users SET points=%s WHERE email=%s",(totalPoints,user_email))
                connection.commit()
                cursor.close()
                connection.close()
                return jsonify({
                    'status':'ok', 
                    'message':'your token added'
                })
        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })  

    def getDeals(self, request):
        
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        query = "SELECT * FROM deals"
        whereForQuery = " where 1 = 1 "
        subQuery = ""
        hasFilters = False

        zip1 = request.args.get("zip")
        if zip1 is not None:
            hasFilters = True
            subQuery = subQuery + " zip = " + "'" + zip1 + "'"
            
        fil = request.args.get("filter_id")
        if fil is not None:
            hasFilters = True
            subQuery = subQuery + "AND" + " filter_id = " + fil 

        lat = request.args.get("latitude")
        if lat is not None:
            hasFilters = True
            subQuery = subQuery + " " +"AND" + " latitude LIKE " + "'" + lat + "%" + "'"

        lag = request.args.get("longitude")
        if lag is not None:
            hasFilters = True
            subQuery = subQuery + " " +"AND" + " longitude LIKE " + "'" + lag + "%" + "'"

        tit = request.args.get("title")
        if tit is not None:
            hasFilters = True
            subQuery = subQuery + " " +"AND" + " title LIKE " + "'" + tit + "%" + "'"

        try:
            if hasFilters == True:
                query = query + whereForQuery + subQuery
            
            print("getDeals Query = ", query)
            cursor.execute(query)
            dealsData = cursor.fetchall()
            dealDict = {
                "status": "ok",
                "message": "deals",
                'data': dealsData,
                }
            cursor.close()
            connection.close()
            return jsonify(dealDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getClaimedDeals(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        query = '''
        SELECT
            deals.*,
            count(claims.claim_id)
        FROM deals
            INNER JOIN claims
            ON claims.deal_id = deals.id
        WHERE 1 = 1
        --x1
        GROUP BY deals.id
        HAVING count(claims.claim_id) > %s
        ''' % request.args.get("min", 0)

        subQuery = ""
        hasFilters = False

        lat = request.args.get("latitude")
        if lat is not None:
            hasFilters = True
            subQuery = subQuery + " " +"AND" + " latitude LIKE " + "'" + lat + "%" + "'"

        lag = request.args.get("longitude")
        if lag is not None:
            hasFilters = True
            subQuery = subQuery + " " +"AND" + " longitude LIKE " + "'" + lag + "%" + "'"

        try:
            query = query.replace('--x1', subQuery)
            
            print("getClaimedDeals Query = ", query)
            cursor.execute(query)
            dealsData = cursor.fetchall()
            dealDict = {
                "status": "ok",
                "message": "deals",
                'data': dealsData,
                }
            cursor.close()
            connection.close()
            return jsonify(dealDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })


    def addFavourites(self, request):       
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            user_email = request.form['user_email']
            deal_id = request.form['deal_id'] 
            cursor.execute("SELECT * FROM favourites WHERE deal_id = %s AND user_email = %s", (deal_id, user_email))
            result = cursor.fetchone()
            if result > 0:
                return jsonify({
                        "status":"fail",
                        "message": " this deal already in your favourites "
                    })
            else:     
                cursor.execute("INSERT INTO favourites (user_email,deal_id) VALUES (%s,%s)",(user_email,deal_id))
                connection.commit()
                cursor.close()
                connection.close()
                return jsonify({
                    'status':'ok', 
                    'message':'your favourite added'
                })
        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getFavourites(self, request):
        email = request.args['email']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("SELECT * FROM deals WHERE id IN (SELECT deal_id FROM favourites WHERE user_email = %s)",(email,))
            favouriteDeals = cursor.fetchall()
            # TODO: Need to change this logic
            for deal in favouriteDeals:
                deal['isFavorite'] = True

            reviewDict = {
                "status": "ok",
                "message": "favouriteDeals",
                'data': favouriteDeals
                }
            cursor.close()
            connection.close()
            return jsonify(reviewDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def addToClaims(self, request):       
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            user_email = request.form['user_email']
            deal_id = request.form['deal_id'] 
            price = request.form['price']
            points = request.form.get('points', 100)
            tid = request.form['transaction']
            # cursor.execute("SELECT * FROM claims WHERE deal_id = %s AND user_email = %s", (deal_id, user_email))
            # result = cursor.fetchone()
            result = 0
            if result > 0:
                return jsonify({
                        "status":"fail",
                        "message": " you'r already claim this deal "
                    })
            else:     
                cursor.execute("INSERT INTO claims (user_email,deal_id, price, transaction) VALUES (%s,%s,%s,%s)",(user_email,deal_id,price,tid))
                connection.commit()

                cursor.close()
                connection.close()
                return jsonify({
                    'status':'ok', 
                    'message':'deal claimed'
                })
        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def addToPayments(self, request):       
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            user_email = request.form['user_email']
            deal_id = request.form['deal_id'] 
            price = request.form['price']
            # cursor.execute("SELECT * FROM payments WHERE deal_id = %s AND user_email = %s", (deal_id, user_email))
            # result = cursor.fetchone()
            result = 0
            if result > 0:
                return jsonify({
                        "status":"fail",
                        "message": " you're already have paid this deal "
                    })
            else:     
                cursor.execute("INSERT INTO payments (user_email,deal_id,price) VALUES (%s,%s,%s)",(user_email,deal_id,price))
                connection.commit()
                cursor.close()
                connection.close()
                return jsonify({
                    'status':'ok', 
                    'message':'deal paid'
                })
        except Exception as e:
            print(e)
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getTokens(self, request):
        email = request.args['email']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("SELECT * FROM tokens WHERE user_email = %s",(email,))
            tokensData = cursor.fetchall()
            cursor.execute("SELECT * FROM deals WHERE id IN (SELECT deal_id FROM tokens WHERE user_email = %s)",(email,))
            dealsdata = cursor.fetchall()
            reviewDict = {
                "status": "ok",
                "message": "data",
                "tokens" : tokensData,
                "deals": dealsdata
                }
            cursor.close()
            connection.close()
            return jsonify(reviewDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getClaims(self, request):
        email = request.args['email']
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("SELECT * FROM deals WHERE id IN (SELECT deal_id FROM claims WHERE user_email = %s)",(email,))
            claimsdata = cursor.fetchall()
            reviewDict = {
                "status": "ok",
                "message": "data",
                "data" : claimsdata,
                }
            cursor.close()
            connection.close()
            return jsonify(reviewDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def removeFavourites(self, request):
        user_email = request.form['user_email']
        deal_id = request.form['deal_id']

        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        try:
            cursor.execute("DELETE FROM favourites WHERE user_email=%s AND deal_id=%s", (user_email, deal_id))
            reviewDict = {
                "status": "ok",
                "message": "favourite removed"
                }
            connection.commit()
            cursor.close()
            connection.close()
            return jsonify(reviewDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getDealsByLatLng(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"

        query = "SELECT * FROM deals"
        whereForQuery = " where 1 = 1 "
        subQuery = ""
        hasFilters = False
            
        lat = request.args.get("latitude")
        if lat is not None:
            ln = int(float(lat))
            lnmin = ln-5
            lnmax = ln+5
            hasFilters = True
            subQuery = subQuery + " AND latitude BETWEEN " + "'" + str(lnmin)  + "'" + "AND" + "'" + str(lnmax) + "'"
        lag = request.args.get("longitude")
        if lag is not None:
            lg = int(float(lag))
            lgmin = lg-5
            lgmax = lg+5
            hasFilters = True
            subQuery = subQuery + " " +"AND" + " longitude BETWEEN " + "'" + str(lgmin) + "'" + "AND"  + "'" + str(lgmax) + "'"

        fil = request.args.get("filter_id")
        if fil is not None:
            hasFilters = True
            subQuery = subQuery + "AND" + " filter_id = " + fil 

        try:
            if hasFilters == True:
                query = query + whereForQuery + subQuery
            
            print("getDeals Query = ", query)

            cursor.execute(query)
            allDeals = cursor.fetchall()

            user_email = request.headers.get("userid")
            if user_email is not None:
                cursor.execute("SELECT deal_id FROM favourites WHERE user_email=%s",(user_email,))
                userFavorites = cursor.fetchall()

                # TODO: Need to corrct this logic in future
                #=============================================
                for deal in allDeals:
                    deal['isFavorite'] = False
                    for fav in userFavorites:
                        if deal['id'] == fav['deal_id']:
                            deal['isFavorite'] = True
                #=============================================

            dealDict = {
                "status": "ok",
                "message": "deals",
                'data':allDeals
                }
            cursor.close()
            connection.close()
            return jsonify(dealDict)

        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getClaimUsers(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        limit = request.args.get('limit')

        query = "SELECT claims.date, users.email, users.first_name, users.last_name, users.phone, users.image u_image, deals.* FROM claims "
        query += "LEFT JOIN users ON users.email = claims.user_email "
        query += "LEFT JOIN deals ON deals.id = claims.deal_id "
        query += "WHERE 1 = 1 "

        lat = request.args.get("latitude")
        if lat is not None:
            ln = int(float(lat))
            lnmin = ln-5
            lnmax = ln+5
            hasFilters = True
            query += " AND deals.latitude BETWEEN " + "'" + str(lnmin)  + "'" + "AND" + "'" + str(lnmax) + "'"
        lag = request.args.get("longitude")
        if lag is not None:
            lg = int(float(lag))
            lgmin = lg-5
            lgmax = lg+5
            hasFilters = True
            query += " AND" + " deals.longitude BETWEEN " + "'" + str(lgmin) + "'" + "AND"  + "'" + str(lgmax) + "'"

        query += "ORDER BY claims.date DESC "
        if limit:
            query += "LIMIT " + str(limit)
        try:
            cursor.execute(query)
            claimsdata = cursor.fetchall()
            reviewDict = {
                "status": "ok",
                "message": "data",
                "data" : claimsdata,
                }
            cursor.close()
            connection.close()
            return jsonify(reviewDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })

    def getPaidUsers(self, request):
        connection = mysql.connector.connect(user=DB.DB_USER, password=DB.DB_PASSWORD)
        cursor = connection.cursor(dictionary=True)
        connection.database = "lunchbox"
        limit = request.args.get('limit')

        query = "SELECT payments.date, payments.price p_price, users.email, users.first_name, users.last_name, users.phone, users.image u_image, deals.* FROM payments "
        query += "LEFT JOIN users ON users.email = payments.user_email "
        query += "LEFT JOIN deals ON deals.id = payments.deal_id "
        query += "WHERE 1 = 1 "

        lat = request.args.get("latitude")
        if lat is not None:
            ln = int(float(lat))
            lnmin = ln-5
            lnmax = ln+5
            hasFilters = True
            query += " AND deals.latitude BETWEEN " + "'" + str(lnmin)  + "'" + "AND" + "'" + str(lnmax) + "'"
        lag = request.args.get("longitude")
        if lag is not None:
            lg = int(float(lag))
            lgmin = lg-5
            lgmax = lg+5
            hasFilters = True
            query += " AND" + " deals.longitude BETWEEN " + "'" + str(lgmin) + "'" + "AND"  + "'" + str(lgmax) + "'"

        query += "ORDER BY payments.date DESC "
        if limit:
            query += "LIMIT " + str(limit)
        try:
            cursor.execute(query)
            claimsdata = cursor.fetchall()
            reviewDict = {
                "status": "ok",
                "message": "data",
                "data" : claimsdata,
                }
            cursor.close()
            connection.close()
            return jsonify(reviewDict)
        except Exception as e:
            print(e)
            return e;
            connection.close()
            return jsonify({
                'status':'fail', 
                'message':'DB Error'
            })


